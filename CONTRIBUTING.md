We consider ourselves as an international community of developers and users keeping Wolfenstein: Enemy Territory alive and up to date.   
Our development is a collaborative effort done in an open, transparent and friendly manner.  
Anyone is welcome to join our efforts! If you want to get in touch with us, please don't hesitate to contact us on our forum at https://dev.etlegacy.com/projects/etlegacy/boards, #etlegacy on IRC freenode or on our Gitter channel etlegacy.

To get started contributing, you need to clone the repository. [Please follow this to get started][1].

[1]: https://gitlab.com/wolfenstein-legacy/et-legacy/wikis/how-to-setup-git